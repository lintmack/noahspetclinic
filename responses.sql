-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 15, 2017 at 01:14 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `responses`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_cform`
--

CREATE TABLE `tb_cform` (
  `ID` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `subj` text NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_cform`
--

INSERT INTO `tb_cform` (`ID`, `name`, `email`, `subj`, `message`) VALUES
(8, 'JonoH', 'jonoh@test.com', 'Home Visit', 'I would like to schedule a home visit'),
(9, 'Jane Smith', 'jane@janesworld.com', 'worming treatments', 'I would like to know more about worming treatments'),
(10, 'Bobby Smith', 'bob@bobsyourmate.com', 'Dog Dental Care', 'Hi I would liek to know more about the dog dental care you offer.'),
(11, 'Suzie', 'suzie@suziesuzie.com', 'puppy inoculations', 'Hi I have just got a puppy and would like to book it in for inoculations.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_cform`
--
ALTER TABLE `tb_cform`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_cform`
--
ALTER TABLE `tb_cform`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
