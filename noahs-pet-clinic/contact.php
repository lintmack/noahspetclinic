<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Noah's Pet Clinic - Contact Us</title>
  <link rel="stylesheet" href="../styles/styles.css">
</head>

<body>
  <nav>
    <a href="../index.html">
      <img id="npc-logo" src="../img/npc-banner.jpg" alt="Noah's Pet Clinic Banner">
    </a>
    <ul>
      <li>
        <a href="../index.html">Home</a>
      </li>
      <li>
        <a href="aboutUs.html">About The Clinic</a>
      </li>
      <li>
        <a href="treatments.html">Treatments</a>
      </li>
      <li>
        <a href="contact.php">Contact</a>
      </li>
    </ul>
  </nav>
  <div class="content">
    <div class="content-two">
      <h1>Contact The Clinic</h1>
      <h2>Fill in your details below</h2>
      <form action="../scripts/thankyou.php" method="post">
        <input type="text" name="name" required placeholder="Enter your name e.g. Janet Smith">
        <br><br>
        <input type="email" name="email" required placeholder="Enter your email address e.g. Janet@Smith.com">
        <br><br>
        <input type="text" name="subj" required placeholder="Enter the subject of your enquiry">
        <br><br>
        <input type="text" name="message" required placeholder="Enter your message here">
        <br><br>
        <p>By clicking submit you agree to share your details with Noah's Pet Clinic 
          and agree to let the clinic contact you.
        </p>
        <button type="submit" value="Submit">Submit</button>
      </form>
    </div>
  </div>
  <footer>
    <div class="column-footer">
      <p>&copy;Noah's Pet Clinic 2017</p>
      <p>Coded By:
        <br> Linton Mackereth</p>
    </div>
    <div class="column-footer">
      <p>
        <a href="../index.html">Home</a>
      </p>
      <p>
        <a href="aboutUs.html">About The Clinic</a>
      </p>
      <p>
        <a href="treatments.html">Treatments</a>
      </p>
      <p>
        <a href="contact.php">Contact</a>
      </p>
    </div>
    <div class="column-footer">
      <p>
        <a href="https://www.facebook.com">Facebook</p>
      <p>
        <a href="https://www.twitter.com">Twitter</p>
      <p>
        <a href="https://www.youtube.com">Youtube</p>
      <p>
        <a href="https://www.linkedin.com">LinkedIn</p>
    </div>
    <div class="column-footer">
      <p>Noah's Pet clinic
        <br> 15 picadilly Gardens
        <br> Manchester
        <br> M1 1ER
        <br> 0161 111 2222
      </p>
    </div>
  </footer>
</body>

</html>