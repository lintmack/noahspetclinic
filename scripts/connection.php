<!-- Please note I have used MAMP and had a password of 
"root" for PHPMyAdmin - Depending upon your settings you may not need
the $dbpass variable in this file. -->

<?php
 
function Connect()
{
 $dbhost = "localhost";
 $dbuser = "root";
 $dbpass = "root";
 $dbname = "responses";
 
 // Create connection
 $conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname) or die($conn->connect_error);
 
 return $conn;
}
 
?>